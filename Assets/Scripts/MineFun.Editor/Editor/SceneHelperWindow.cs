﻿using System.Collections.Generic;
using System.Linq;
using MineFun.Hexes;
using UnityEngine;
using UnityEditor;

namespace MineFun
{
    public class SceneHelperWindow : EditorWindow
    {
        private void OnGUI()
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Hexes", EditorStyles.centeredGreyMiniLabel);

            if (GUILayout.Button("Align selected hexes by grid"))
            {
                AlignSelected();
            }

            if (GUILayout.Button("Align all hexes by grid"))
            {
                AlignAll();
            }
        }

        private void AlignSelected()
        {
            var objects = Selection.gameObjects.Select(x => x.transform);
            AlignObjects(objects);
        }

        private void AlignAll()
        {
            var objects = FindObjectsOfType<HexRoot>().Select(x => x.transform);
            AlignObjects(objects);
        }

        private void AlignObjects(IEnumerable<Transform> roots)
        {
            foreach (var root in roots)
            {
                root.position = HexHelper.GetNearestCenter(root.position);
            }
        }

        [MenuItem("Develop/Scene Helper")]
        private static void OpenWindow()
        {
            GetWindow<SceneHelperWindow>("Scene Helper");
        }
    }
}