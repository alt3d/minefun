﻿using System.Collections.Generic;
using UnityEngine;

public static class UResources
{
    public static IEnumerable<T> LoadAll<T>() where T : Object
    {
        return Resources.LoadAll<T>(string.Empty);
    }

    public static IEnumerable<T> LoadAll<T>(string path) where T : Object
    {
        return Resources.LoadAll<T>(path);
    }

    public static T Singleton<T>() where T : Object
    {
        Debug.Assert(Resources.LoadAll<T>(string.Empty).Length == 1);

        return Resources.LoadAll<T>(string.Empty)[0];
    }

    public static GameObject Load(string path)
    {
        Debug.Assert(Resources.Load<GameObject>(path) != null, path);

        return Resources.Load<GameObject>(path);
    }

    public static T Load<T>(string path) where T : ScriptableObject
    {
        Debug.Assert(path != null);
        Debug.Assert(!string.IsNullOrEmpty(path));
        Debug.Assert(Resources.Load<T>(path) != null, path);

        return Resources.Load<T>(path);
    }
}