﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public static class UGameObjects
{
    public static GameObject Find(string name)
    {
        Debug.Assert(!string.IsNullOrWhiteSpace(name));
        return GameObject.Find(name);
    }

    public static IEnumerable<GameObject> FindInRoots(string name)
    {
        Debug.Assert(!string.IsNullOrWhiteSpace(name));
        return GetSceneRoots().Where(x => x.name == name);
    }
    
    public static List<Transform> GetChildren(Transform root)
    {
        Debug.Assert(root != null);

        var result = new List<Transform>();
        foreach (Transform child in root)
        {
            result.Add(child);
        }

        return result;
    }

    public static void DeleteChildren(Transform root)
    {
        Debug.Assert(root);

        var childCount = root.childCount;
        for (int i = childCount - 1; i >= 0; i--)
        {
            var child = root.GetChild(i);
            child.gameObject.SetActive(false);
            child.SetParent(null, false);
            Object.Destroy(child.gameObject);
        }
    }

    public static GameObject CreateEmpty(string name)
    {
        Debug.Assert(name.NotNull());

        return new GameObject(name);
    }

    public static GameObject CreateEmpty(string name, GameObject parent)
    {
        Debug.Assert(name.NotNull());
        Debug.Assert(parent != null);
        
        return CreateEmpty(name, parent.transform);
    }
    
    public static GameObject CreateEmpty(string name, Transform parent)
    {
        Debug.Assert(name.NotNull());
        Debug.Assert(parent != null);

        var result = new GameObject(name);
        result.transform.parent = parent;
        return result;
    }

    public static void Delete(GameObject gameObject)
    {
        if (!gameObject) 
            return;
        
        gameObject.SetActive(false);
        Object.Destroy(gameObject);
    }

    public static void Delete(Component component)
    {
        if (!component)
            return;
        
        Delete((component.gameObject));
    }

    public static GameObject Instantiate(GameObject prefab)
    {
        return Instantiate(prefab, Vector3.zero, Quaternion.identity, null);
    }
    
    public static GameObject Instantiate(GameObject prefab, Transform parent)
    {
        return Instantiate(prefab, parent.position, parent.rotation, parent);
    }

    public static GameObject Instantiate(GameObject prefab, Vector3 position)
    {
        return Instantiate(prefab, position, Quaternion.identity, null);
    }

    public static GameObject Instantiate(GameObject prefab, Vector3 position, Transform parent)
    {
        return Instantiate(prefab, position, Quaternion.identity, parent);
    }

    public static GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        return Instantiate(prefab, position, rotation, null);
    }

    public static GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
    {
        Debug.Assert(prefab != null);

        if (prefab == null)
            return null;

        var go = Object.Instantiate(prefab, position, rotation, parent);
        go.name = prefab.name;
        return go;
    }

    public static T Singleton<T>() where T : MonoBehaviour
    {
        Debug.Assert(Object.FindObjectsOfType<T>().Length == 1);
        
        return Object.FindObjectOfType<T>();
    }

    public static GameObject[] GetSceneRoots()
    {
        return SceneManager.GetActiveScene().GetRootGameObjects();
    }
}
