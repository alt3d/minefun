﻿using System.Collections;
using Alt3d.Engine.Coroutines;
using UnityEngine;

namespace Alt3d
{
	public static class ACoroutines 
	{
        private static CoroutinesHandler _handler;

        private static CoroutinesHandler Handler
        {
            get
            {
                if (_handler == null)
                {
                    var go = new GameObject("COROUTINES");
                    _handler = go.AddComponent<CoroutinesHandler>();
                }

                return _handler;
            }
        }

        public static Coroutine Run(IEnumerator enumerator)
        {
            return Handler.StartCoroutine(enumerator);
        }

        public static Coroutine Kill(Coroutine coroutine)
        {
            if (coroutine != null)
            {
                Handler.StopCoroutine(coroutine);
            }

            return null;
        }
    }
}