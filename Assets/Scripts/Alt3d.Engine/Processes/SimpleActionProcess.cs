﻿namespace Alt3d
{	
	public abstract class SimpleActionProcess : ProcessBase
	{
		// ======================================================================

		public abstract void Run();

		// ======================================================================
	}
}