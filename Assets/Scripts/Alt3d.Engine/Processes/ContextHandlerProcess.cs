﻿namespace Alt3d
{	
	public abstract class ContextHandlerProcess<TContext, THandler> : ProcessBase, IProcess<THandler>
	{
		// ======================================================================

		protected readonly AList<THandler> Handlers;

		// ======================================================================
		
		public void Add(THandler handler)
		{
			Handlers.Add(handler);
		}

		public abstract TContext CreateContext();
		
		public abstract void Run(TContext context);
		
		// ======================================================================

		protected ContextHandlerProcess()
		{
			Handlers = new AList<THandler>();
		}
		
		// ======================================================================
	}
}