﻿using System;

namespace Alt3d
{	
	public abstract class ProcessBase : IProcess
	{	
		// ======================================================================

		public event Action Begin = delegate { };
		
		public event Action Finish = delegate { };

		// ======================================================================

		protected void RiseBegin() => Begin();
		
		protected void RiseFinish() => Finish();
		
		// ======================================================================
	}
}