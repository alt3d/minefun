﻿namespace Alt3d
{	
	public abstract class ContextActionProcess<TContext> : ProcessBase
	{
		// ======================================================================

		public abstract TContext CreateContext();
		
		public abstract void Run(TContext context);

		// ======================================================================
	}
}