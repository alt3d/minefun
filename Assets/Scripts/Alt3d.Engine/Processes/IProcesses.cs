﻿using System;

namespace Alt3d
{	
	// ======================================================================

	public interface IProcess
	{
		event Action Begin;

		event Action Finish;
	}
	
	public interface IProcess<in THandler> : IProcess
	{
		void Add(THandler handler);
	}
	
	// ======================================================================
}