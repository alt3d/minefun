﻿namespace Alt3d
{	
	public abstract class SimpleHandlerProcess<THandler> : ProcessBase, IProcess<THandler>
	{
		// ======================================================================

		protected readonly AList<THandler> Handlers;

		// ======================================================================
		
		public void Add(THandler handler)
		{
			Handlers.Add(handler);
		}
		
		public abstract void Run();
		
		// ======================================================================

		protected SimpleHandlerProcess()
		{
			Handlers = new AList<THandler>();
		}
		
		// ======================================================================
	}
}