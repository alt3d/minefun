﻿using UnityEngine;
using UnityEditor;

namespace Alt3d
{
	public class MenuEditorWindow : EditorWindow
	{
        // ======================================================================

        private void OnGUI()
        {
            var guiColor = GUI.color;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Common", EditorStyles.centeredGreyMiniLabel);
            if (GUILayout.Button("Close All"))
            {
                CloseAll();
            }

            var canvases = FindObjectsOfType<MonoCanvas>();
            foreach (var canvas in canvases)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField(canvas.name, EditorStyles.centeredGreyMiniLabel);

                var menus = canvas.GetComponentsInChildren<MenuContent>(true);
                foreach (var menu in menus)
                {
                    GUI.color = menu.IsOpen ? Color.yellow : guiColor;

                    if (GUILayout.Button(menu.name))
                    {
                        Toggle(menu);
                    }
                }
            }

            GUI.color = guiColor;
        }

        private void CloseAll()
        {
            HideAll();
            Selection.activeGameObject = null;
        }

        private void Toggle(MenuContent menu)
        {
            HideAll();
            menu.Show();
            Selection.activeGameObject = menu.gameObject;
        }

        private void HideAll()
        {
            var menus = FindObjectsOfType<MenuContent>();
            foreach (var menu in menus)
            {
                menu.Hide();
            }
        }

        // ======================================================================

        //[MenuItem("Develop/Menu Editor")]
        private static void Open()
        {
            GetWindow<MenuEditorWindow>("Menu Editor");
        }

		// ======================================================================
	}
}