﻿using System;
using UnityEngine;

public class ATrigger : MonoBehaviour
{
    // ======================================================================

    public event Action<Collider> OverlapStart;

    public event Action<Collider> OverlapFinish;

    // ======================================================================

    private void OnTriggerEnter(Collider other)
    {
        OverlapStart?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        OverlapFinish?.Invoke(other);
    }

    // ======================================================================
}
