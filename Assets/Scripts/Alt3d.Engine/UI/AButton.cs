﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class AButton : MonoBehaviour
{
    // ======================================================================

    private Button _button;

    public Button Button
    {
        get
        {
            if (_button == null)
            {
                _button = GetComponent<Button>();
                Debug.Assert(_button != null, this);
            }

            return _button;
        }
    }

    // ======================================================================

    public bool Interactable
    {
        get { return Button.interactable; }
        set
        {
            Button.interactable = value;
            InteractableChanged();
        }
    }

    // ======================================================================

    public event Action Clicked = delegate { };

    public event Action<AButton> ButtonClicked = delegate { };

    public event Action InteractableChanged = delegate { };

    // ======================================================================

    private void OnEnable()
    {
        Button.onClick.AddListener(OnButtonClick);
    }

    private void OnDisable()
    {
        Button.onClick.RemoveListener(OnButtonClick);
    }

    protected virtual void OnButtonClick()
    {
        Clicked();
        ButtonClicked(this);
    }

    // ======================================================================
}