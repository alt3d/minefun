﻿using UnityEngine;

public abstract class ALabel : MonoBehaviour
{
    public abstract string Value { get; set; }

    public abstract Color Color { get; set; }


    public void SetValue(object value)
    {
        Value = value != null ? value.ToString() : string.Empty;
    }

    public abstract void ToggleDraw(bool isVisible);
}