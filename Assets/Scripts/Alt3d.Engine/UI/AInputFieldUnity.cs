﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d.Engine.Native
{
    public class AInputFieldUnity : AInputField
    {
        [SerializeField] private InputField _inputField;

        public InputField InputField
        {
            get
            {
                if (!_inputField)
                {
                    _inputField = GetComponent<InputField>();
                    Debug.Assert(_inputField, this);
                }
                
                return _inputField;
            }
        }

        public override string Value
        {
            get => InputField.text;
            set => InputField.text = value;
        }

        private void OnValidate()
        {
            _inputField = GetComponent<InputField>();
        }
        
        private void OnEnable()
        {
            InputField.onEndEdit.AddListener(OnEndEdit);
            InputField.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnDisable()
        {
            InputField.onEndEdit.RemoveListener(OnEndEdit);
            InputField.onValueChanged.RemoveListener(OnValueChanged);
        }
        
        private void OnEndEdit(string value)
        {
            RiseEndEdit();
        }

        private void OnValueChanged(string value)
        {
            RiseValueChanged();
        }
    }
}