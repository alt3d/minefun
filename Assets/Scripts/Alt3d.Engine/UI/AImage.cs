﻿using UnityEngine;
using UnityEngine.UI;

public class AImage : MonoBehaviour
{
    private Image _image;

    public Image Image
    {
        get
        {
            if (_image == null)
            {
                _image = GetComponent<Image>();
                Debug.Assert(_image != null, this);
            }

            return _image;
        }
    }

    public Sprite Sprite
    {
        get => Image.sprite;
        set => Image.sprite = value;
    }

    public Color Color
    {
        get => Image.color;
        set => Image.color = value;
    }
    
    public void ToggleDraw(bool isVisible)
    {
        Image.enabled = isVisible;
    }
}