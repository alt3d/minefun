﻿using UnityEngine;
using UnityEngine.UI;

namespace Alt3d.Engine.Native
{
    public class ALabelUnity : ALabel
    {
        private Text _text;

        public Text Text
        {
            get
            {
                if (_text == null)
                {
                    _text = GetComponent<Text>();
                    Debug.Assert(_text != null, this);
                }

                return _text;
            }
        }

        public override string Value
        {
            get => Text.text;
            set => Text.text = value;
        }

        public override Color Color
        {
            get => Text.color;
            set => Text.color = value;
        }

        public override void ToggleDraw(bool isVisible)
        {
            Text.enabled = isVisible;
        }
    }
}