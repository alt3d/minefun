﻿using UnityEngine;
using TMPro;

namespace Alt3d.Engine.TMP
{
    public class ALabelTMP : ALabel
    {
        private TMP_Text _text;

        public TMP_Text Text
        {
            get
            {
                if (_text == null)
                {
                    _text = GetComponent<TMP_Text>();
                    Debug.Assert(_text);
                }

                return _text;
            }
        }

        public override string Value
        {
            get => Text.text;
            set => Text.text = value;
        }

        public override Color Color
        {
            get => Text.color;
            set => Text.color = value;
        }

        public override void ToggleDraw(bool isVisible)
        {
            Text.enabled = isVisible;
        }
    }
}