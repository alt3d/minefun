﻿using System;
using UnityEngine;

public abstract class AInputField : MonoBehaviour
{
    public abstract string Value { get; set; }
    
    public event Action<AInputField> EndEdit;

    public event Action<AInputField> ValueChanged;

    protected void RiseEndEdit()
    {
        EndEdit?.Invoke(this);
    }

    protected void RiseValueChanged()
    {
        ValueChanged?.Invoke(this);
    }
}