﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ASlider : MonoBehaviour
{
    // ======================================================================

    private Slider _slider;

    public Slider Slider
    {
        get
        {
            if (_slider == null)
            {
                _slider = GetComponent<Slider>();
                Debug.Assert(_slider != null, this);
            }

            return _slider;
        }
    }

    // ======================================================================

    public event Action ValueChanged = delegate { };

    public event Action<ASlider> SliderValueChanged = delegate { };

    // ======================================================================

    public float Value
    {
        get => Slider.value;
        set => Slider.value = value;
    }

    public int RoundValue
    {
        get => Mathf.RoundToInt(Slider.value);
        set => Slider.value = value;
    }

    public float NormalizedValue
    {
        get => Slider.normalizedValue;
        set => Slider.normalizedValue = value;
    }

    public float MinValue
    {
        get => Slider.minValue;
        set => Slider.minValue = value;
    }
    
    public float MaxValue
    {
        get => Slider.maxValue;
        set => Slider.maxValue = value;
    }

    // ======================================================================

    private void OnEnable()
    {
        Slider.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnDisable()
    {
        Slider.onValueChanged.RemoveListener(OnValueChanged);
    }

    private void OnValueChanged(float value)
    {
        ValueChanged();
        SliderValueChanged(this);
    }

    // ======================================================================
}
