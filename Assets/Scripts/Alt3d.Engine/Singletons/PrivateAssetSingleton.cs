﻿using UnityEngine;

public abstract class PrivateAssetSingleton<TAsset> : AssetSingletonBase<TAsset> where TAsset : ScriptableObject
{
	protected static TAsset Instance
	{
		get
		{
			if (!_instance)
			{
				Create();
			}
            
			return _instance;
		}
	}
}