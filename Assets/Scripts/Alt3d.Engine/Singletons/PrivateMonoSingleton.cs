﻿using UnityEngine;

public abstract class PrivateMonoSingleton<TMono> : MonoSingletonBase<TMono> where TMono : MonoBehaviour
{
    protected static TMono Instance
    {
        get
        {
            if (!_instance)
            {
                Create();
            }
            
            return _instance;
        }
    }
}