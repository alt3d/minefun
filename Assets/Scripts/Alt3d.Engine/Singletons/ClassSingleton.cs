﻿using System;
using UnityEngine;

public abstract class ClassSingleton<TClass> where TClass : class
{
    private static TClass _instance;

    public static TClass Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Activator.CreateInstance<TClass>();
                Debug.Assert(_instance != null);
            }
                
            return _instance;
        }
    }
}
