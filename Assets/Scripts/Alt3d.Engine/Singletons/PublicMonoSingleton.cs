﻿using UnityEngine;

public abstract class PublicMonoSingleton<TMono> : MonoSingletonBase<TMono> where TMono : MonoBehaviour
{
    public static TMono Instance
    {
        get
        {
            if (!_instance)
            {
                Create();
            }
            
            return _instance;
        }
    }
}