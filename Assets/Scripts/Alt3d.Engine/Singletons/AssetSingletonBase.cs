﻿using System;
using System.Linq;
using UnityEngine;

public abstract class AssetSingletonBase<TAsset> : ScriptableObject where TAsset : ScriptableObject
{
    protected static TAsset _instance { get; private set; }
    
    protected static void Create()
    {
        var attribute = Attribute.GetCustomAttribute(typeof(TAsset), typeof(AssetSingletonAttribute)) as AssetSingletonAttribute;
        if (attribute == null || attribute.Path == null || string.IsNullOrWhiteSpace(attribute.Path))
        {
            _instance = Load();
        }
        else
        {
            _instance = Load(attribute.Path);
        }
                
        Debug.Assert(_instance, typeof(TAsset).FullName);
    }

    private static TAsset Load()
    {
        Debug.Assert(
            Resources.LoadAll<TAsset>(string.Empty).Length > 0,
            $"{typeof(TAsset).FullName}: {Resources.LoadAll<TAsset>(string.Empty).Length}");
        
        return Resources.LoadAll<TAsset>(string.Empty).First();
    }
    
    private static TAsset Load(string path)
    {
        Debug.Assert(
            Resources.Load<TAsset>(path),
            $"{typeof(TAsset).FullName}: {path}");
        
        return Resources.Load<TAsset>(path);
    }
}