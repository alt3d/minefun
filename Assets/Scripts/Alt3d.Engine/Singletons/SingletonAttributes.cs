﻿using System;

[AttributeUsage(AttributeTargets.Class)]
public class MonoSingletonAttribute : Attribute
{
    public readonly bool IsAutoCreate;

    public readonly bool IsDontDestroy;

    public readonly string PrefabPath; 

    public MonoSingletonAttribute(bool isAutoCreate = false, bool isDontDestroy = false, string prefabPath = null)
    {
        IsAutoCreate = isAutoCreate;
        IsDontDestroy = isDontDestroy;
        PrefabPath = prefabPath;
    }
}
    
[AttributeUsage(AttributeTargets.Class)]
public class AssetSingletonAttribute : Attribute
{
    public readonly string Path;

    public AssetSingletonAttribute(string path = null)
    {
        Path = path;
    }
}