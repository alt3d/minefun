﻿using System;
using System.Linq;
using UnityEngine;

public abstract class MonoSingletonBase<TMono> : MonoBehaviour where TMono : MonoBehaviour
{
    protected static TMono _instance { get; private set; }
    
    protected static void Create()
    {
        var type = typeof(TMono);
        var attribute = Attribute.GetCustomAttribute(type, typeof(MonoSingletonAttribute)) as MonoSingletonAttribute;

        if (attribute == null)
        {
            _instance = Find();
        }
        else
        {
            if (attribute.IsAutoCreate)
            {
                _instance = Create(type);
            }
            else if (attribute.PrefabPath != null)
            {
                _instance = Create(attribute.PrefabPath);
            }
            else
            {
                _instance = Find();
            }
                    
            if (_instance && _instance != null && attribute.IsDontDestroy)
            {
                DontDestroyOnLoad(_instance.gameObject);
            }
        }
        
        Debug.Assert(_instance, type.FullName);
        Debug.Assert(FindObjectsOfType<TMono>().Length == 1);
    }
    
    private static TMono Create(string path)
    {
        var prefab = UResources.Load(path);
        var go = UGameObjects.Instantiate(prefab);
        return go.GetComponent<TMono>();
    }

    private static TMono Create(Type type)
    {
        var go = new GameObject(type.Name);
        return go.AddComponent<TMono>();
    }

    private static TMono Find()
    {
        Debug.Assert(FindObjectsOfType<TMono>().Length == 1,
            $"{typeof(TMono).FullName}: {FindObjectsOfType<TMono>().Length}");     
        
        return FindObjectsOfType<TMono>().First();
    }
}