﻿using System;
using System.Linq;
using UnityEngine;

public abstract class PublicAssetSingleton<TAsset> : AssetSingletonBase<TAsset> where TAsset : ScriptableObject
{
	public static TAsset Instance
	{
		get
		{
			if (!_instance)
			{
				Create();
			}
            
			return _instance;
		}
	}
}