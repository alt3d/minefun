﻿using System;

namespace Alt3d
{
	// ======================================================================

	public interface IMenu
	{
		bool IsOpen { get; }

        event Action Opened;
        
        event Action Closed;

		event Action<IMenu> MenuOpened;
		
		event Action<IMenu> MenuClosed;

        void Close();
    }

	// ======================================================================
}