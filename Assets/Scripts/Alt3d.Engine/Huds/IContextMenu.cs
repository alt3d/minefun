namespace Alt3d
{
    // ======================================================================

    public interface IContextMenu<TContext> : IMenu
    {
        TContext Context { get; }

        void Open(TContext to);
    }
    
    // ======================================================================
}