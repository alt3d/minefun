﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Alt3d
{
	public class MenuContent : MonoBehaviour
	{
		// ======================================================================

		[FormerlySerializedAs("_content")] 
		[SerializeField] protected GameObject Сontent;

        // ======================================================================

        public bool IsOpen => Сontent.activeSelf;

        // ======================================================================

        public void Show() => Сontent.SetActive(true);

        public void Hide() => Сontent.SetActive(false);

        // ======================================================================
    }
}