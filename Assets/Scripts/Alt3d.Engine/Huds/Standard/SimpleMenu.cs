﻿namespace Alt3d
{
	public abstract class SimpleMenu : MonoMenu, ISimpleMenu
	{
		// ======================================================================

		public void Open()
        {
            DoOpen();
        }

		public void Toggle()
		{
			if (!IsOpen)
			{
				Open();
			}
			else
			{
				Close();
			}
		}

		// ======================================================================
	}
}