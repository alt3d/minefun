﻿using UnityEngine;

namespace Alt3d
{
	public class MonoCanvas : MonoBehaviour
	{
        // ======================================================================

        [SerializeField] protected bool _isDebug;

        // ======================================================================

        public bool IsDebug => _isDebug;

        // ======================================================================
    }
}