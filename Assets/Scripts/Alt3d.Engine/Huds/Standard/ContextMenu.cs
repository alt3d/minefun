﻿namespace Alt3d
{
    public abstract class ContextMenu<TContext> : MonoMenu, IContextMenu<TContext>
    {
        // ======================================================================

        public TContext Context { get; private set; }

        // ======================================================================

        public void Open(TContext context)
        {
            Context = context;
            DoOpen();
        }

        // ======================================================================
    }
}