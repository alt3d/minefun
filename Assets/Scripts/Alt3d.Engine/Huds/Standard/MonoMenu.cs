﻿using System;
using UnityEngine;

namespace Alt3d
{
	public abstract class MonoMenu: MonoBehaviour, IMenu
	{
        // ======================================================================

        private RectTransform _root;
        private MenuContent _content;
        
        // ======================================================================
       
        private RectTransform Root
        {
            get
            {
                if (_root == null)
                    _root = transform as RectTransform;

                return _root;
            }
        }

        private MenuContent Content
        {
            get
            {
                if (_content == null)
                {
                    _content = GetComponent<MenuContent>();
                    Debug.Assert(_content != null);
                }

                return _content;
            }
        }

        public bool IsOpen => Content.IsOpen;

        // ======================================================================

        public event Action Opened;

        public event Action Closed;

        public event Action<IMenu> MenuOpened;

        public event Action<IMenu> MenuClosed;

        // ======================================================================

        protected void Awake()
        {
            Content.Hide();
        }

        public void Close()
        {
            DoClose();
        }

        protected void DoOpen()
        {
            Root.SetAsLastSibling();
            BeforeOpen();
            Content.Show();
            Opened?.Invoke();
            MenuOpened?.Invoke(this);
        }

        protected void DoClose()
        {
            Content.Hide();
            AfterClose();
            Closed?.Invoke();
            MenuClosed?.Invoke(this);
        }

        protected virtual void BeforeOpen() { }

        protected virtual void AfterClose() { }

        // ======================================================================
    }
}