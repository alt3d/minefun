﻿using UnityEngine;

public static class ExtComponents
{
    public static T GetInParent<T>(this Component self) where T : MonoBehaviour
    {
        Debug.Assert(self);
        Debug.Assert(self.GetComponentInParent<T>());
        
        return self.GetComponentInParent<T>();
    }
    
    public static T GetSelf<T>(this Component self) where T : MonoBehaviour
    {
        Debug.Assert(self);
        Debug.Assert(self.GetComponent<T>());
        
        return self.GetComponent<T>();
    }
    
    public static T GetInChildren<T>(this Component self) where T : MonoBehaviour
    {
        Debug.Assert(self);
        Debug.Assert(self.GetComponentInChildren<T>());
        
        return self.GetComponentInChildren<T>();
    }
}