﻿using System;

public static class ExtMisc
{
    public static bool IsNull(this Guid guid)
    {
        return guid == Guid.Empty;
    }

    public static bool NotNull(this Guid guid)
    {
        return !IsNull(guid);
    }

    public static bool IsNull(this string value)
    {
        if (value == null)
        {
            return false;
        }
        
        return string.IsNullOrWhiteSpace(value);
    }

    public static bool NotNull(this string value)
    {
        return !IsNull(value);
    }
}
