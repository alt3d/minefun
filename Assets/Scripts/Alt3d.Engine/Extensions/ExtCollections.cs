﻿using System.Collections.Generic;
using System.Linq;
using Random = System.Random;

public static class ExtCollections
{
    public static TItem GetRandomItem<TItem>(this IEnumerable<TItem> collection)
    {
        var count = collection.Count();
        if (count == 0)
        {
            return default;
        }
        
        var index = UnityEngine.Random.Range(0, count);
        return collection.ElementAt(index);
    }
    
    public static TItem GetRandomItem<TItem>(this IEnumerable<TItem> collection, Random random)
    {
        var count = collection.Count();
        if (count == 0)
        {
            return default;
        }
        
        var index = random.Next(0, count);
        return collection.ElementAt(index);
    }
}
