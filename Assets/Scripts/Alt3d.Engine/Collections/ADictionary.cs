﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class ADictionary<TKey, TValue>
	{
		// ======================================================================

		private readonly Dictionary<TKey, TValue> Collection = new Dictionary<TKey, TValue>();

		// ======================================================================

		public int Count => Collection.Count;

		public IEnumerable<TKey> Keys => Collection.Keys;

		public IEnumerable<TValue> Values => Collection.Values;

		public IEnumerable<KeyValuePair<TKey, TValue>> Pairs => Collection;

		// ======================================================================

		public event Action<TKey> Added = delegate { };
		
		public event Action<TKey> Removed = delegate { };

		// ======================================================================

		public bool Contains(TKey key)
		{
			if (key == null)
			{
				Debug.Assert(false);
				return false;
			}

			return Collection.ContainsKey(key);
		}

		public TValue Get(TKey key)
		{
			if (!Contains(key))
			{
				return default;
			}

			return Collection[key];
		}
		
		public bool Add(TKey key, TValue value)
		{
			if (key == null)
			{
				Debug.Assert(false);
				return false;
			}
			
			if (value == null)
			{
				Debug.Assert(false);
				return false;
			}

			if (Collection.ContainsKey(key))
			{
				Debug.Assert(false, value);
				return false;
			}
			
			Collection.Add(key, value);
			Added(key);
			return true;
		}

		public bool Remove(TKey key)
		{
			if (key == null)
			{
				Debug.Assert(false);
				return false;
			}

			if (!Collection.ContainsKey(key))
			{
				Debug.Assert(false, key);
				return false;
			}

			Collection.Remove(key);
			Removed(key);
			return true;
		}

		public void Clear()
		{
			Collection.Clear();
		}
		
		// ======================================================================
	}
}