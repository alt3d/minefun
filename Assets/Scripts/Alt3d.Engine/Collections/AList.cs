﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class AList<TItem> : IEnumerable<TItem>
	{
		// ======================================================================

		private readonly List<TItem> Collection = new List<TItem>();

		// ======================================================================

		public int Count => Collection.Count;

		public IEnumerable<TItem> Values => Collection;

		// ======================================================================

		public event Action<TItem> Added = delegate { };
		
		public event Action<TItem> Removed = delegate { };

		// ======================================================================

		public bool Contains(TItem item)
		{
			if (item == null)
			{
				Debug.Assert(false);
				return false;
			}

			return Collection.Contains(item);
		}
		
		public bool Add(TItem item)
		{
			if (item == null)
			{
				Debug.Assert(false, typeof(TItem).ToString());
				return false;
			}

			if (Collection.Contains(item))
			{
				Debug.Assert(false, item);
				return false;
			}
			
			Collection.Add(item);
			Added(item);
			return true;
		}

		public bool Add(IEnumerable<TItem> collection)
		{
			if (collection == null)
			{
				Debug.Assert(false);
				return false;
			}

			var result = false;
			foreach (var item in collection)
			{
				if (!result)
				{
					result = Add(item);
				}
				else
				{
					Add(item);
				}
			}

			return result;
		}

		public bool Remove(TItem item)
		{
			if (item == null)
			{
				Debug.Assert(false);
				return false;
			}

			if (!Collection.Contains(item))
			{
				Debug.Assert(false, item);
				return false;
			}

			Collection.Remove(item);
			Removed(item);
			return true;
		}

		public void Clear()
		{
			Collection.Clear();
		}
		
		// ======================================================================
		
		public IEnumerator<TItem> GetEnumerator()
		{
			return Collection.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		
		// ======================================================================
	}
}