﻿using UnityEngine;

namespace MineFun.Equips
{
    public interface IEquipItem
    {
        void Activate(IEquipAgent agent);

        void Deactivate(IEquipAgent agent);
    }
    
    public interface IEquipAgent
    {
        Transform HandRoot { get; }
        
        IEquipItem Current { get; }

        void Change(IEquipItem item);
    }
}