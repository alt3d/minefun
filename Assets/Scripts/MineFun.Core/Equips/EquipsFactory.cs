﻿using MineFun.Equips.Equips;
using MineFun.Hexes;
using UnityEngine;

namespace MineFun.Equips
{
    public class EquipsFactory : PrivateMonoSingleton<EquipsFactory>
    {
        [SerializeField] private MainEquipsFactory Main;

        [SerializeField] private HexEquipsFactory Hexes;

        public static IEquipItem GetEmpty()
        {
            return Instance.Main.GetEmpty();
        }
        
        public static IEquipItem GetPickAxe()
        {
            return Instance.Main.GetPickAxe();
        }

        public static IEquipItem GetHex(HexId id)
        {
            return Instance.Hexes.GetEquip(id);
        }
    }
}