﻿using UnityEngine;

namespace MineFun.Equips
{
    public class EquipAgent : MonoBehaviour, IEquipAgent
    {
        [SerializeField] private Transform _handRoot;

        [SerializeField] private EquipItem _item;

        public Transform HandRoot => _handRoot;

        public IEquipItem Current
        {
            get => _item;
            private set => _item = value as EquipItem;
        }

        public void Change(IEquipItem item)
        {
            Current?.Deactivate(this);
            Current = item;
            Current?.Activate(this);
        }
    }
}