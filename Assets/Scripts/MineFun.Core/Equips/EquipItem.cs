﻿using UnityEngine;

namespace MineFun.Equips
{
    public abstract class EquipItem : MonoBehaviour, IEquipItem
    {
        protected IEquipAgent Agent { get; private set; }
        
        protected bool IsActive { get; private set; }

        public void Activate(IEquipAgent agent)
        {
            Agent = agent;
            IsActive = true;
            OnActivate();
        }

        public void Deactivate(IEquipAgent agent)
        {
            OnDeactivate();
            IsActive = false;
            Agent = null;
        }

        protected abstract void OnActivate();

        protected abstract void OnDeactivate();
    }
}