﻿using MineFun.Hexes;
using UnityEngine;

namespace MineFun.Equips.Equips
{
    public class HexEquipItem : EquipItem
    {
        private Transform Preview;

        public HexDef HexDef { get; set; }

        protected override void OnActivate()
        {
            var root = Agent.HandRoot;

            var go = UGameObjects.Instantiate(HexDef.Prefab, root);
            go.layer = 0;

            var colliders = go.GetComponentsInChildren<Collider>();
            foreach (var cldr in colliders)
            {
                cldr.enabled = false;
            }

            Preview = go.transform;
        }

        protected override void OnDeactivate()
        {
            if (Preview)
            {
                UGameObjects.Delete(Preview);
            }
        }

        private void LateUpdate()
        {
            if (!IsActive)
            {
                return;
            }

            var center = Vector3.zero;
            center.x = Screen.width * 0.5f;
            center.y = Screen.height * 0.5f;
            var ray = Camera.main.ScreenPointToRay(center);

            var mask = 1 << 10; // TODO Глобальный класс
            var distance = 10f; // TODO В настройки

            var isAttach = false;
            if (Physics.Raycast(ray, out var hit, distance, mask))
            {
                var body = hit.collider.attachedRigidbody;
                if (body)
                {
                    var hex = body.GetComponent<HexRoot>();
                    if (hex)
                    {
                        var side = HexSides.GetSide(hit.normal);
                        if (side.IsVertical())
                        {
                            if (side == HexSide.Top)
                            {
                                center = hit.point + side.Vector() * HexSettings.HalfHeight * 0.8f;
                            }
                            else
                            {
                                center = hit.point + side.Vector() * HexSettings.HalfHeight * 2f;
                            }
                        }
                        else
                        {
                            center = hit.point + side.Vector() * HexSettings.Radius;
                        }

                        center = HexHelper.GetNearestCenter(center);
                        Preview.position = center;
                        Preview.rotation = Quaternion.identity;

                        isAttach = true;
                        UDraw.Line(ray.origin, hit.point, Color.green);
                        UDraw.Line(hit.point, hit.point + hit.normal, Color.yellow);
                    }
                }
            }

            if (!isAttach)
            {
                center = HexHelper.GetNearestCenter(ray.origin + ray.direction * (HexSettings.Radius * 5f));
                Preview.position = center;
                Preview.rotation = Quaternion.identity;
            }

            if (Input.GetKeyDown(KeyCode.Mouse2))
            {
                var prefab = HexDef.Prefab;
                var go = UGameObjects.Instantiate(prefab);
                var index = HexHelper.GetIndex(center);
                var hex = HexWorld.Get(index);
                if (hex == null)
                {
                    go.transform.position = HexHelper.GetCenter(index);

                    hex = go.GetComponent<HexRoot>();
                    hex.Index = index;
                    HexWorld.Add(hex);
                }
            }
        }
    }
}