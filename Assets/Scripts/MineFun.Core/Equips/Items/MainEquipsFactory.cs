﻿using UnityEngine;

namespace MineFun.Equips.Equips
{
    public class MainEquipsFactory : MonoBehaviour
    {
        [SerializeField] private EmptyEquipItem Empty;

        [SerializeField] private PickAxeEquipItem PickAxe;

        public IEquipItem GetEmpty()
        {
            return Empty;
        }

        public IEquipItem GetPickAxe()
        {
            return PickAxe;
        }
    }
}