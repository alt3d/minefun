﻿using MineFun.Hexes;
using UnityEngine;

namespace MineFun.Equips.Equips
{
    public class PickAxeEquipItem : EquipItem
    {
        [SerializeField] private GameObject CagePrefab;

        private Transform Cage;

        protected override void OnActivate()
        {
            var root = Agent.HandRoot;
            var go = UGameObjects.Instantiate(CagePrefab, root);
            Cage = go.transform;
        }

        protected override void OnDeactivate()
        {
            if (Cage)
            {
                UGameObjects.Delete(Cage);
            }
        }

        private void LateUpdate()
        {
            if (!IsActive)
            {
                return;
            }

            var center = Vector3.zero;
            center.x = Screen.width * 0.5f;
            center.y = Screen.height * 0.5f;
            var ray = Camera.main.ScreenPointToRay(center);

            var mask = 1 << 10; // TODO Глобальный класс
            var distance = 10f; // TODO В настройки

            var isExists = false;
            if (Physics.Raycast(ray, out var hit, distance, mask))
            {
                var body = hit.collider.attachedRigidbody;
                if (body)
                {
                    var hex = body.GetComponent<HexRoot>();
                    if (hex)
                    {
                        center = hex.transform.position;
                        Cage.position = center;
                        Cage.rotation = Quaternion.identity;

                        var index = HexHelper.GetIndex(center);
                        isExists = HexWorld.Contains(index);

                        UDraw.Line(ray.origin, center, Color.green);
                    }
                }
            }

            Cage.gameObject.SetActive(isExists);

            if (Input.GetKeyDown(KeyCode.Mouse2))
            {
                var index = HexHelper.GetIndex(center);
                var hex = HexWorld.Get(index);
                if (hex)
                {
                    HexWorld.Remove(hex);
                    UGameObjects.Delete(hex);
                }
            }
        }
    }
}