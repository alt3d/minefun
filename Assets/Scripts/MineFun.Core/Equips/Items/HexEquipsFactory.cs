﻿using System.Collections.Generic;
using MineFun.Hexes;
using UnityEngine;

namespace MineFun.Equips.Equips
{
    public class HexEquipsFactory : MonoBehaviour
    {
        [SerializeField] private GameObject Prefab;

        private Dictionary<HexId, HexEquipItem> Collection;

        private void Awake()
        {
            Collection = new Dictionary<HexId, HexEquipItem>();

            foreach (var id in HexIds.All)
            {
                var go = UGameObjects.Instantiate(Prefab);
                go.transform.SetParent(transform);
                go.name = $"{id}.Equip";
                
                var equip = go.GetComponent<HexEquipItem>();
                equip.HexDef = HexDefs.Get(id);

                Collection.Add(id, equip);
            }
        }

        public IEquipItem GetEquip(HexId id)
        {
            return Collection[id];
        }
    }
}