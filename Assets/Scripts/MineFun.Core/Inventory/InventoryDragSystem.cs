﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MineFun.Inventories
{
    public class InventoryDragSystem : PrivateMonoSingleton<InventoryDragSystem>
    {
        [SerializeField] private Canvas Canvas;

        [SerializeField] private InventoryDragItem Item;

        [SerializeField] private RectTransform ItemRect;

        private static Vector3 BeginOffset;
        
        private static IInventoryItem CurrentItem;

        private void Awake()
        {
            Canvas.enabled = false;
        }

        public static void OnDragBegin(IInventorySlot slot, PointerEventData eventData)
        {
            CurrentItem = slot.Item;
            if (CurrentItem == null)
            {
                return;;
            }
            
            Instance.Canvas.enabled = true;
            
            Instance.Item.Icon = slot.Item.Icon;
            Instance.ItemRect.position = slot.Root.position;

            var pointerPosition = Input.mousePosition;
            BeginOffset = pointerPosition - Instance.ItemRect.position;
        }

        public static void OnDrag(IInventorySlot slot, PointerEventData eventData)
        {
            if (CurrentItem == null)
            {
                return;;
            }
            
            Instance.ItemRect.position = Input.mousePosition + BeginOffset;
        }

        public static void OnDragEnd(IInventorySlot slot, PointerEventData eventData)
        {
            if (CurrentItem == null)
            {
                return;;
            }
            
            var list = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, list);

            var target = GetSlot(list);
            if (target != null)
            {
                Swap(slot, target);
            }

            Instance.Canvas.enabled = false;
        }

        private static void Swap(IInventorySlot from, IInventorySlot to)
        {
            var fromItem = from.Item;
            var toItem = to.Item;

            from.ReplaceItem(toItem);
            to.ReplaceItem(fromItem);
        }

        private static IInventorySlot GetSlot(IReadOnlyList<RaycastResult> list)
        {
            if (list.Count == 0)
            {
                return null;
            }

            var go = list[0].gameObject;
            if (!go)
            {
                return null;
            }

            return go.GetComponent<IInventorySlot>();
        }
    }
}