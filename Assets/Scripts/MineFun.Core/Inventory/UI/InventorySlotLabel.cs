﻿using UnityEngine;

namespace MineFun.Inventories.UIs
{
    public class InventorySlotLabel : InventorySlotUI
    {
        [SerializeField] private ALabel CountLabel;

        protected override void Redraw(IInventoryItem item)
        {
            if (item == null || !item.IsStackable)
            {
                CountLabel.ToggleDraw(false);
                return;
            }

            CountLabel.SetValue(item.Count);
            CountLabel.ToggleDraw(true);
        }
    }
}