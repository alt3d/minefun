﻿using UnityEngine;

namespace MineFun.Inventories.UIs
{
    public abstract class InventorySlotUI : MonoBehaviour
    {
        private IInventorySlot Slot;

        protected void Awake()
        {
            Slot = GetComponent<IInventorySlot>();
        }

        protected void OnEnable()
        {
            Slot.ItemChanged += OnItemChanged;
            Redraw(Slot.Item);
        }

        protected void OnDisable()
        {
            Slot.ItemChanged -= OnItemChanged;
        }

        private void OnItemChanged(IInventorySlot slot)
        {
            Redraw(Slot.Item);
        }

        protected abstract void Redraw(IInventoryItem item);
    }
}