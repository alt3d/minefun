﻿using UnityEngine;

namespace MineFun.Inventories.UIs
{
    public class InventorySlotIcon : InventorySlotUI
    {
        [SerializeField] private AImage IconImage;

        protected override void Redraw(IInventoryItem item)
        {
            if (item == null)
            {
                IconImage.ToggleDraw(false);
                return;
            }

            IconImage.Sprite = item.Icon;
            IconImage.ToggleDraw(true);
        }
    }
}