﻿using System.Collections.Generic;
using UnityEngine;

namespace MineFun.Inventories
{
    public class Inventory : MonoBehaviour, IInventory
    {
        private List<IInventorySlot> Collection;

        public int Capacity => Collection.Count;

        public IEnumerable<IInventorySlot> Slots => Collection;

        public IInventorySlot GetSlot(int index)
        {
            return Collection[index];
        }

        public void Init(IEnumerable<IInventorySlot> slots)
        {
            Collection = new List<IInventorySlot>(slots);
        }
    }
}