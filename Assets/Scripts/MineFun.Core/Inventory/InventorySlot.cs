﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MineFun.Inventories
{
    public class InventorySlot : MonoBehaviour, IInventorySlot, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private AButton Button;

        [SerializeField] private RectTransform Rect;

        public RectTransform Root => Rect;

        public IInventory Inventory { get; private set; }

        public int Index { get; private set; }

        public bool IsEmpty => Item == null;

        public IInventoryItem Item { get; private set; }

        public event Action<IInventorySlot> ItemChanged;

        public void Init(IInventory inventory, int index)
        {
            Inventory = inventory;
            Index = index;
        }

        public void ReplaceItem(IInventoryItem item)
        {
            Item = item;
            ItemChanged?.Invoke(this);
        }

        private void OnEnable()
        {
            Button.Clicked += ButtonOnClicked;
        }

        private void OnDisable()
        {
            Button.Clicked -= ButtonOnClicked;
        }

        private void ButtonOnClicked()
        {
            Item?.OnClicked();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            InventoryDragSystem.OnDragBegin(this, eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            InventoryDragSystem.OnDrag(this, eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            InventoryDragSystem.OnDragEnd(this, eventData);
        }
    }
}