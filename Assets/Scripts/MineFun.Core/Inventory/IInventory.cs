﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MineFun.Inventories
{
    public interface IInventoryItem
    {
        int Capacity { get; }

        int Count { get; }
        
        bool IsStackable { get; }
        
        Sprite Icon { get; }
        
        Action Action { get; set; }

        void OnClicked();
    }

    public interface IInventorySlot
    {
        RectTransform Root { get; }
        
        IInventory Inventory { get; }
        
        int Index { get; }
        
        bool IsEmpty { get; }
        
        IInventoryItem Item { get; }

        void Init(IInventory inventory, int index);
        
        void ReplaceItem(IInventoryItem item);

        event Action<IInventorySlot> ItemChanged;
    }

    public interface IInventory
    {
        int Capacity { get; }

        IEnumerable<IInventorySlot> Slots { get; }

        void Init(IEnumerable<IInventorySlot> slots);

        IInventorySlot GetSlot(int index);
    }
}