﻿using System;
using UnityEngine;

namespace MineFun.Inventories
{
    public class InventoryItem : IInventoryItem
    {
        public int Capacity { get; set; }

        public int Count { get; set; }

        public bool IsStackable => Capacity > 1;
        
        public Sprite Icon { get; set; }
        
        public Action Action { get; set; }
        
        public void OnClicked()
        {
            Action?.Invoke();
        }
    }
}