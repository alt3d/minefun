﻿using MineFun.Equips;
using MineFun.Hexes;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MineFun.Inventories.Items
{
    public class InventoryFactory : PrivateMonoSingleton<InventoryFactory>
    {
        // TEMP
        [SerializeField] private Sprite PickAxeIcon;

        // TEMP
        [SerializeField] private Sprite HandIcon;

        public static IInventoryItem GenerateHex(HexId id, IEquipAgent agent)
        {
            var def = HexDefs.Get(id);
            return new InventoryItem()
            {
                Icon = def.Icon,
                Capacity = def.StackCapacity,
                Count = Random.Range(def.StackCapacity / 3, def.StackCapacity),
                Action = () => agent.Change(EquipsFactory.GetHex(id))
            };
        }

        public static IInventoryItem GeneratePickAxe(IEquipAgent agent)
        {
            return new InventoryItem()
            {
                Icon = Instance.PickAxeIcon,
                Capacity = 1,
                Count = 1,
                Action = () => agent.Change(EquipsFactory.GetPickAxe())
            };
        }

        public static IInventoryItem GenerateEmpty(IEquipAgent agent)
        {
            return new InventoryItem()
            {
                Icon = Instance.HandIcon,
                Capacity = 1,
                Count = 1,
                Action = () => agent.Change(EquipsFactory.GetEmpty())
            };
        }
    }
}