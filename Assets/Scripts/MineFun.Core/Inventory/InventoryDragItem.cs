﻿using UnityEngine;

namespace MineFun.Inventories
{
    public class InventoryDragItem : MonoBehaviour
    {
        [SerializeField] private AImage Image;

        public Sprite Icon
        {
            get => Image.Sprite;
            set => Image.Sprite = value;
        }
    }
}