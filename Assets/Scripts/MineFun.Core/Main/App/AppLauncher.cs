﻿using System.Collections;
using UnityEngine;

namespace MineFun.Apps
{
    public class AppLauncher : MonoBehaviour
    {
        [SerializeField] private float FakeLoadDuration = 2f;
        
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(FakeLoadDuration);
                        
            ScenesManager.LoadGame();
        }
    }
}