﻿using System.Collections;
using MineFun.UIs;
using UnityEngine;

namespace MineFun.Apps.UIs
{
    public class AppLoadingProgressWidget : MonoBehaviour
    {
        [SerializeField] private float StepDelay = 0.25f;

        [SerializeField] private ProgressIcon[] Icons;

        private int CurrentIndex;

        private void OnEnable()
        {
            StartCoroutine(DoStep());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private void Redraw()
        {
            for (var i = 0; i < Icons.Length; i++)
            {
                Icons[i].Toggle(i <= CurrentIndex);
            }
        }

        private IEnumerator DoStep()
        {
            CurrentIndex = 0;
            Redraw();
            
            while (enabled)
            {
                yield return new WaitForSeconds(StepDelay);

                CurrentIndex++;
                if (CurrentIndex >= Icons.Length)
                {
                    CurrentIndex = 0;
                }
                
                Redraw();
            }
        }
    }
}