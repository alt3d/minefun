﻿using UnityEngine;

namespace MineFun.UIs
{
    public class ProgressIcon : MonoBehaviour
    {
        [SerializeField] private AImage Image;

        [SerializeField] private Sprite ActiveSprite;

        [SerializeField] private Color ActiveColor = Color.white;

        [SerializeField] private Sprite InactiveSprite;

        [SerializeField] private Color InactiveColor = Color.white;

        public void Toggle(bool isActive)
        {
            Image.Sprite = isActive ? ActiveSprite : InactiveSprite;
            Image.Color = isActive ? ActiveColor : InactiveColor;
        }
    }
}