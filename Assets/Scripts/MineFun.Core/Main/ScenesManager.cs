﻿using UnityEngine.SceneManagement;

namespace MineFun
{
    public static class ScenesManager
    {
        public static void LoadGame()
        {
            SceneManager.LoadScene(1);
        }

        public static void LoadLevel()
        {
            SceneManager.LoadScene(2);
        }
    }
}