﻿using UnityEngine;

namespace MineFun.Games.UIs
{
    public class GameStartMenu : MonoBehaviour
    {
        [SerializeField] private AButton LaunchButton;

        private void OnEnable()
        {
            LaunchButton.Clicked += OnLaunchButtonOnClicked;
        }

        private void OnDisable()
        {
            LaunchButton.Clicked -= OnLaunchButtonOnClicked;
        }

        private void OnLaunchButtonOnClicked()
        {
            ScenesManager.LoadLevel();
        }
    }
}