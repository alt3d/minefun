﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MineFun.Core.Main.Level
{
    public class LevelInitState : MonoBehaviour
    {
        private static readonly List<IEnumerator> Collection = new List<IEnumerator>();
        
        public static IEnumerable<IEnumerator> Actions => Collection;
        
        public static event Action Finish;

        public static void RiseFinish() => Finish?.Invoke();
        
        private void OnDestroy()
        {
            Collection.Clear();
        }

        public static void AddToInitActions(IEnumerator action)
        {
            Collection.Add(action);
        }
    }
}