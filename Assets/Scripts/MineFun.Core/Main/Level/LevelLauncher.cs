﻿using System.Collections;
using MineFun.Core.Main.Level;
using UnityEngine;

namespace MineFun.Levels
{
    public class LevelLauncher : MonoBehaviour
    {
        private IEnumerator Start()
        {
            yield return null;
            yield return null;
            yield return null;

            var actions = LevelInitState.Actions;
            foreach (var action in actions)
            {
                if (action != null)
                {
                    yield return action;
                }
            }

            yield return null;
            LevelInitState.RiseFinish();
        }
    }
}