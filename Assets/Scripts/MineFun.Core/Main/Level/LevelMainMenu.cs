﻿using UnityEngine;

namespace MineFun.Levels.UIs
{
    public class LevelMainMenu : MonoBehaviour
    {
        [SerializeField] private AButton QuitButton;

        private void OnEnable()
        {
            QuitButton.Clicked += OnQuitButtonOnClicked;
        }

        private void OnDisable()
        {
            QuitButton.Clicked -= OnQuitButtonOnClicked;
        }

        private void OnQuitButtonOnClicked()
        {
            ScenesManager.LoadGame();
        }
    }
}