﻿using UnityEngine;

namespace MineFun.Avatars
{
    public class AvatarPhysicsSettings : MonoBehaviour
    {
        [SerializeField] private float _pushPower = 2f;

        public float PushPower => _pushPower;
    }
}