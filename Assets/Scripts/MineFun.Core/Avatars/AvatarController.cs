﻿using System;
using UnityEngine;

namespace MineFun.Avatars
{
    public class AvatarController : MonoBehaviour
    {
        [SerializeField] private CharacterController CharacterController;

        public bool IsGrounded => CharacterController.isGrounded;

        public event Action<ControllerColliderHit> ColliderHit;
        
        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            ColliderHit?.Invoke(hit);
        }

        public void Move(Vector3 direction)
        {
            CharacterController.Move(direction);
        }
    }
}