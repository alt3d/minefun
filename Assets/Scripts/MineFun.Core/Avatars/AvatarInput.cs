﻿using System.Collections;
using MineFun.Core.Main.Level;
using UnityEngine;

namespace MineFun.Avatars
{
    public class AvatarInput : MonoBehaviour
    {
        public float Move { get; private set; }
        
        public float Slide { get; private set; }

        public float Pan { get; private set; }

        public float Tilt { get; private set; }
        
        public bool IsJump { get; private set; }
        
        public bool IsRun { get; private set; }

        private bool IsStartLock;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            IsStartLock = true;
        }

        private void OnEnable()
        {
            LevelInitState.Finish += OnLevelFinish;
        }

        private void OnDisable()
        {
            LevelInitState.Finish -= OnLevelFinish;
        }

        private void OnLevelFinish()
        {
            StartCoroutine(DoStartDelay());
        }

        private IEnumerator DoStartDelay()
        {
            yield return new WaitForSeconds(0.25f);
            IsStartLock = false;
        }

        private void Update()
        {
            if (IsStartLock)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (Cursor.lockState != CursorLockMode.Locked)
                {
                    Cursor.lockState = CursorLockMode.Locked;
                }
                else
                {
                    Cursor.lockState = CursorLockMode.None;
                }
            }
            
            Move = Input.GetAxis("Vertical");
            Slide = Input.GetAxis("Horizontal");
            
            Pan = Input.GetAxis("Mouse X");
            Tilt = Input.GetAxis("Mouse Y");
            
            IsJump = Input.GetKey(KeyCode.Space);
            IsRun = Input.GetKey(KeyCode.LeftShift);
        }
    }
}