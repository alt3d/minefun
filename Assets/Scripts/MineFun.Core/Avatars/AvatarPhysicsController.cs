﻿using UnityEngine;

namespace MineFun.Avatars
{
    public class AvatarPhysicsController : MonoBehaviour
    {
        [SerializeField] private AvatarController AvatarController;

        [SerializeField] private AvatarPhysicsSettings PhysicsSettings;

        private void OnEnable()
        {
            AvatarController.ColliderHit += OnAvatarControllerHit;
        }

        private void OnDisable()
        {
            AvatarController.ColliderHit -= OnAvatarControllerHit;
        }

        private void OnAvatarControllerHit(ControllerColliderHit hit)
        {
            var otherBody = hit.rigidbody;
            if (!otherBody || otherBody.isKinematic)
            {
                return;
            }

            if (hit.moveDirection.y < -0.3)
            {
                return;
            }

            var pushDirection = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
            otherBody.velocity = pushDirection * PhysicsSettings.PushPower;
        }
    }
}