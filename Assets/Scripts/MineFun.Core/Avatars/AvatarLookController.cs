﻿using UnityEngine;

namespace MineFun.Avatars
{
    public class AvatarLookController : MonoBehaviour
    {
        [SerializeField] private AvatarInput AvatarInput;

        [SerializeField] private AvatarMoveSettings MoveSettings;

        [SerializeField] private Transform BodyRoot;

        [SerializeField] private Transform HeadRoot;

        private void Update()
        {
            var dt = Time.deltaTime;
            
            var bodyAngle = AvatarInput.Pan * MoveSettings.PanSpeed * dt;
            var rootRotation = Quaternion.AngleAxis(bodyAngle, Vector3.up);
            BodyRoot.rotation *= rootRotation;

            var angles = HeadRoot.localEulerAngles;
            angles.x -= AvatarInput.Tilt * MoveSettings.TiltSpeed * dt;
            angles.x = UVectors.ClampAngle(angles.x, MoveSettings.MinTiltAngle, MoveSettings.MaxTiltAngle);
            HeadRoot.localEulerAngles = angles;
        }
    }
}