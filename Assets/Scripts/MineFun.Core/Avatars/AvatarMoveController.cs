﻿using UnityEngine;

namespace MineFun.Avatars
{
    public class AvatarMoveController : MonoBehaviour
    {
        [SerializeField] private AvatarController AvatarController;
        
        [SerializeField] private AvatarInput AvatarInput;

        [SerializeField] private AvatarMoveSettings MoveSettings;

        [SerializeField] private Transform PlayerRoot;

        private Vector3 MoveDirection;

        private void Update()
        {
            var dt = Time.deltaTime;

            if (AvatarController.IsGrounded)
            {
                MoveDirection = Vector3.zero;
                MoveDirection += PlayerRoot.forward * (AvatarInput.Move * MoveSettings.MoveSpeed);
                MoveDirection += PlayerRoot.right * (AvatarInput.Slide * MoveSettings.SlideSpeed);

                if (AvatarInput.IsRun)
                {
                    MoveDirection *= MoveSettings.RunMultiplier;
                }

                if (AvatarInput.IsJump)
                {
                    MoveDirection.y = MoveSettings.JumpSpeed;
                }
            }

            MoveDirection.y -= MoveSettings.FallSpeed * dt;
            AvatarController.Move(MoveDirection * dt);
        }
    }
}