﻿using UnityEngine;

namespace MineFun.Avatars
{
    public class AvatarMoveSettings : MonoBehaviour
    {
        [Header("Move Speed")]
        
        [SerializeField] private float _moveSpeed = 6f;

        [SerializeField] private float _slideSpeed = 5f;

        [SerializeField] private float _runMultiplier = 2f;
        
        [Header("Rotate Speed")] 
        
        [SerializeField] private float _panSpeed = 250f;

        [SerializeField] private float _tiltSpeed = 250f;
        
        [Header("Jump")]

        [SerializeField] private float _jumpSpeed = 10f;

        [SerializeField] private float _fallSpeed = 20f;

        [Header("Limitations")] 
        
        [SerializeField] private float _minTiltAngle = -80;

        [SerializeField] private float _maxTiltAngle = 80;

        public float MoveSpeed => _moveSpeed;

        public float SlideSpeed => _slideSpeed;

        public float RunMultiplier => _runMultiplier;

        public float PanSpeed => _panSpeed;

        public float TiltSpeed => _tiltSpeed;

        public float JumpSpeed => _jumpSpeed;

        public float FallSpeed => _fallSpeed;
      
        public float MinTiltAngle => _minTiltAngle;

        public float MaxTiltAngle => _maxTiltAngle;
    }
}