﻿using System;
using UnityEngine;

namespace MineFun.Hexes
{
    public enum HexDirection
    {
        NorthEast = 0,
        East = 1,
        SouthEast = 2,
        SouthWest = 3,
        West = 4,
        NorthWest
    }

    public static class HexDirections
    {
        private static readonly Vector3 NorthEast = Quaternion.AngleAxis(30, Vector3.up) * Vector3.forward;

        private static readonly Vector3 East = Quaternion.AngleAxis(90, Vector3.up) * Vector3.forward;

        private static readonly Vector3 SouthEast = Quaternion.AngleAxis(150, Vector3.up) * Vector3.forward;

        private static readonly Vector3 SouthWest = Quaternion.AngleAxis(210, Vector3.up) * Vector3.forward;

        private static readonly Vector3 West = Quaternion.AngleAxis(270, Vector3.up) * Vector3.forward;

        private static readonly Vector3 NorthWest = Quaternion.AngleAxis(330, Vector3.up) * Vector3.forward;

        public static Vector3 Vector(this HexDirection direction)
        {
            switch (direction)
            {
                case HexDirection.NorthEast: return NorthEast;
                case HexDirection.East: return East;
                case HexDirection.SouthEast: return SouthEast;
                case HexDirection.SouthWest: return SouthWest;
                case HexDirection.West: return West;
                case HexDirection.NorthWest: return NorthWest;
                default:
                    throw new Exception(direction.ToString());
            }
        }

        public static HexDirection GetNext(this HexDirection direction)
        {
            switch (direction)
            {
                case HexDirection.NorthEast: return HexDirection.East;
                case HexDirection.East: return HexDirection.SouthEast;
                case HexDirection.SouthEast: return HexDirection.SouthWest;
                case HexDirection.SouthWest: return HexDirection.West;
                case HexDirection.West: return HexDirection.NorthWest;
                case HexDirection.NorthWest: return HexDirection.NorthEast;
                default:
                    throw new Exception(direction.ToString());
            }
        }
        
        public static HexDirection GetPrevious(this HexDirection direction)
        {
            switch (direction)
            {
                case HexDirection.NorthWest: return HexDirection.West;
                case HexDirection.West: return HexDirection.SouthWest;
                case HexDirection.SouthWest: return HexDirection.SouthEast;
                case HexDirection.SouthEast: return HexDirection.East;
                case HexDirection.East: return HexDirection.NorthEast;
                case HexDirection.NorthEast: return HexDirection.NorthWest;
                default:
                    throw new Exception(direction.ToString());
            }
        }
    }
}