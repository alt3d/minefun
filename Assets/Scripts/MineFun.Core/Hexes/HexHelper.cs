﻿using UnityEngine;

namespace MineFun.Hexes
{
    public static class HexHelper
    {
        private static float Radius => HexSettings.Radius;

        private static float Height => HexSettings.Height;

        private static float DistanceX => HexMath.DistanceX(Radius);

        private static float DistanceY => HexMath.DistanceY(Height);

        private static float DistanceZ => HexMath.DistanceZ(Radius);

        public static Vector3Int GetIndex(Vector3 point)
        {
            var index = Vector3Int.zero;
            index.x = Mathf.RoundToInt(point.x / DistanceX);
            index.y = Mathf.RoundToInt(point.y / DistanceY);
            index.z = Mathf.RoundToInt(point.z / DistanceZ);

            if (HexMath.IsOdd(index.z))
            {
                index.x = Mathf.RoundToInt((point.x - Radius) / DistanceX);
            }

            return index;
        }

        public static Vector3 GetCenter(Vector3Int index)
        {
            var center = Vector3.zero;
            center.x = index.x * DistanceX;
            center.y = index.y * DistanceY;
            center.z = index.z * DistanceZ;

            if (HexMath.IsOdd(index.z))
            {
                center.x += Radius;
            }

            return center;
        }

        public static Vector3 GetNearestCenter(Vector3 point)
        {
            var index = GetIndex(point);
            return GetCenter(index);
        }
    }
}