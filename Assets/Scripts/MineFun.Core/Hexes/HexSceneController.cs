﻿using UnityEngine;

namespace MineFun.Hexes
{
    public class HexSceneController : MonoBehaviour
    {
        private void Start()
        {
            var array = FindObjectsOfType<HexRoot>();
            foreach (var hex in array)
            {
                var center = hex.transform.position;
                hex.Index = HexHelper.GetIndex(center);
                HexWorld.Add(hex);
            }
        }
    }
}