﻿using UnityEngine;

namespace MineFun.Hexes
{
    [SelectionBase]
    public class HexRoot : MonoBehaviour
    {
        [Header("Debug")]
        [SerializeField] private Vector3Int _index;

        public Vector3Int Index
        {
            get => _index;
            set => _index = value;
        }
    }
}