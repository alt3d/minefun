﻿using UnityEngine;

namespace MineFun.Hexes
{
    public class HexSettings : PrivateAssetSingleton<HexSettings>
    {
        [SerializeField] private float _radius = 0.5f;

        [SerializeField] private float _height = 0.5f;

        public static float Radius => Instance._radius;

        public static float Height => Instance._height;

        public static float HalfHeight => Height * 0.5f;
    }
}