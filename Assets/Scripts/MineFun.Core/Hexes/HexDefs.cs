﻿using System.Collections.Generic;

namespace MineFun.Hexes
{
    public class HexDefs : PrivateAssetSingleton<HexDefs>
    {
        private static readonly Dictionary<HexId, HexDef> Collection;

        public static HexDef Get(HexId id)
        {
            if (!Collection.ContainsKey(id))
            {
                var def = UResources.Load<HexDef>($"Hexes/Defs/{id}");
                Collection.Add(id, def);
            }

            return Collection[id];
        }

        static HexDefs()
        {
            Collection = new Dictionary<HexId, HexDef>();
        }
    }
}