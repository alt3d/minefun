﻿using UnityEngine;

namespace MineFun.Hexes
{
    public class HexDef : ScriptableObject
    {
        [SerializeField] private GameObject _prefab;

        [SerializeField] private Sprite _icon;

        [SerializeField] private int _stackCapacity;

        public string Id => name;
        
        public GameObject Prefab => _prefab;

        public Sprite Icon => _icon;

        public int StackCapacity => _stackCapacity;
    }
}