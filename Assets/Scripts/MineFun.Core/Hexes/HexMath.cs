﻿namespace MineFun.Hexes
{
    public static class HexMath
    {
        private static float InnerToOuterRadius(float innerRadius)
        {
            return innerRadius / 0.866025404f;
        }

        public static float DistanceX(float radius)
        {
            return radius * 2f;
        }

        public static float DistanceY(float height)
        {
            return height;
        }

        public static float DistanceZ(float radius)
        {
            return InnerToOuterRadius(radius) * 1.5f;
        }

        public static bool IsOdd(int z)
        {
            return (z & 1) == 1;
        }
    }
}