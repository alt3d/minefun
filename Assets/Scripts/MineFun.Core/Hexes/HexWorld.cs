﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MineFun.Hexes
{
    public static class HexWorld
    {
        private static readonly Dictionary<Vector3Int, HexRoot> Collection;

        public static event Action<HexRoot> Added;

        public static event Action<HexRoot> Removed;

        public static bool Contains(Vector3Int index)
        {
            return Collection.ContainsKey(index);
        }

        public static HexRoot Get(Vector3Int index)
        {
            if (!Collection.ContainsKey(index))
            {
                return null;
            }

            return Collection[index];
        }

        public static void Add(HexRoot hex)
        {
            if (!hex)
            {
                Debug.LogWarning("Hex is null");
                return;
            }

            if (Collection.ContainsKey(hex.Index))
            {
                Debug.LogWarning($"Already contains: {hex.Index}", hex);
                return;
            }

            Collection.Add(hex.Index, hex);
            Added?.Invoke(hex);
        }

        public static void Remove(HexRoot hex)
        {
            if (!hex)
            {
                Debug.LogWarning("Hex is null");
                return;
            }

            if (!Collection.ContainsKey(hex.Index))
            {
                Debug.LogWarning($"Not contains: {hex.Index}", hex);
                return;
            }

            Collection.Remove(hex.Index);
            Removed?.Invoke(hex);
        }

        static HexWorld()
        {
            Collection = new Dictionary<Vector3Int, HexRoot>();
        }
    }
}