﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

namespace MineFun.Hexes
{
    public enum HexId
    {
        Grass,
        Log,
        Sand,
        Soil,
        Stone,
        Wood
    }

    public static class HexIds
    {
        public static IEnumerable<HexId> All { get; } = Enum.GetValues(typeof(HexId)).Cast<HexId>();

        public static HexId GetRandom()
        {
            var list = All.ToList();
            var index = Random.Range(0, list.Count);
            return list[index];
        }
    }
}