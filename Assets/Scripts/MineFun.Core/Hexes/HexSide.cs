﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MineFun.Hexes
{
    public enum HexSide
    {
        Top,
        Bottom,
        NorthEast,
        East,
        SouthEast,
        SouthWest,
        West,
        NorthWest
    }

    public static class HexSides
    {
        public static readonly IEnumerable<HexSide> All = new List<HexSide>()
        {
            HexSide.Top,
            HexSide.Bottom,
            HexSide.NorthEast,
            HexSide.East,
            HexSide.SouthEast,
            HexSide.SouthWest,
            HexSide.West,
            HexSide.NorthWest
        };

        private static readonly Vector3 Top = Vector3.up;

        private static readonly Vector3 Bottom = Vector3.down;

        private static readonly Vector3 NorthEast = Quaternion.AngleAxis(30, Vector3.up) * Vector3.forward;

        private static readonly Vector3 East = Quaternion.AngleAxis(90, Vector3.up) * Vector3.forward;

        private static readonly Vector3 SouthEast = Quaternion.AngleAxis(150, Vector3.up) * Vector3.forward;

        private static readonly Vector3 SouthWest = Quaternion.AngleAxis(210, Vector3.up) * Vector3.forward;

        private static readonly Vector3 West = Quaternion.AngleAxis(270, Vector3.up) * Vector3.forward;

        private static readonly Vector3 NorthWest = Quaternion.AngleAxis(330, Vector3.up) * Vector3.forward;

        public static HexSide GetSide(Vector3 normal)
        {
            var dot = Vector3.Dot(normal, Vector3.up);
            if (Mathf.Abs(dot) > 0.5f)
            {
                return dot > 0 ? HexSide.Top : HexSide.Bottom;
            }

            var result = HexSide.Top;
            var maxDot = Mathf.NegativeInfinity;
            foreach (var side in All)
            {
                dot = Vector3.Dot(normal, side.Vector());
                if (dot > maxDot)
                {
                    result = side;
                    maxDot = dot;
                }
            }

            return result;
        }

        public static bool IsVertical(this HexSide side)
        {
            switch (side)
            {
                case HexSide.Top:
                case HexSide.Bottom:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsHorizontal(this HexSide side)
        {
            return !side.IsVertical();
        }

        public static Vector3 Vector(this HexSide side)
        {
            switch (side)
            {
                case HexSide.Top: return Top;
                case HexSide.Bottom: return Bottom;
                case HexSide.NorthEast: return NorthEast;
                case HexSide.East: return East;
                case HexSide.SouthEast: return SouthEast;
                case HexSide.SouthWest: return SouthWest;
                case HexSide.West: return West;
                case HexSide.NorthWest: return NorthWest;
                default:
                    throw new Exception(side.ToString());
            }
        }

        public static HexSide GetNext(this HexSide side)
        {
            switch (side)
            {
                case HexSide.Top: return HexSide.NorthEast;
                case HexSide.NorthEast: return HexSide.East;
                case HexSide.East: return HexSide.SouthEast;
                case HexSide.SouthEast: return HexSide.SouthWest;
                case HexSide.SouthWest: return HexSide.West;
                case HexSide.West: return HexSide.NorthWest;
                case HexSide.NorthWest: return HexSide.Bottom;
                case HexSide.Bottom: return HexSide.Top;
                default:
                    throw new Exception(side.ToString());
            }
        }

        public static HexSide GetPrevious(this HexSide side)
        {
            switch (side)
            {
                case HexSide.Bottom: return HexSide.NorthWest;
                case HexSide.NorthWest: return HexSide.West;
                case HexSide.West: return HexSide.SouthWest;
                case HexSide.SouthWest: return HexSide.SouthEast;
                case HexSide.SouthEast: return HexSide.East;
                case HexSide.East: return HexSide.NorthEast;
                case HexSide.NorthEast: return HexSide.Top;
                case HexSide.Top: return HexSide.Bottom;
                default:
                    throw new Exception(side.ToString());
            }
        }
    }
}