﻿using System.Linq;
using MineFun.Equips;
using MineFun.Hexes;
using MineFun.Inventories;
using MineFun.Inventories.Items;
using UnityEngine;

namespace MineFun.Players
{
    public class PlayerInventory : MonoBehaviour
    {
        [SerializeField] private EquipAgent EquipAgent;

        [SerializeField] private Inventory ToolBelt;

        [SerializeField] private Inventory BackPack;

        private void Awake()
        {
            {
                var inventory = BackPack;

                var slots = inventory.GetComponentsInChildren<IInventorySlot>(true);
                for (var i = 0; i < slots.Length; i++)
                {
                    slots[i].Init(inventory, i);
                    slots[i].ReplaceItem(null);
                }

                inventory.Init(slots);

                var index = 0;
                foreach (var id in HexIds.All)
                {
                    var item = InventoryFactory.GenerateHex(id, EquipAgent);
                    var slot = inventory.GetSlot(index);
                    slot.ReplaceItem(item);

                    index++;
                }
            }

            {
                var inventory = ToolBelt;

                var slots = inventory.GetComponentsInChildren<IInventorySlot>(true);
                for (var i = 0; i < slots.Length; i++)
                {
                    slots[i].Init(inventory, i);
                    slots[i].ReplaceItem(null);
                }

                inventory.Init(slots);

                var item = InventoryFactory.GenerateEmpty(EquipAgent);
                var slot = inventory.GetSlot(0);
                slot.ReplaceItem(item);

                for (var i = 1; i < slots.Length - 1; i++)
                {
                    var id = HexIds.GetRandom();
                    item = InventoryFactory.GenerateHex(id, EquipAgent);
                    slot = inventory.GetSlot(i);
                    slot.ReplaceItem(item);
                }

                item = InventoryFactory.GeneratePickAxe(EquipAgent);
                slot = inventory.GetSlot(slots.Length - 1);
                slot.ReplaceItem(item);
            }

            BackPack.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                BackPack.gameObject.SetActive(!BackPack.gameObject.activeSelf);
            }
        }
    }
}