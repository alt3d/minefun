﻿Shader "Debug/PBR" 
{
	Properties 
	{
		_Color("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex("Albedo", 2D) = "white" {}

		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
		[NoScaleOffset] _MetallicGlossMap("Metallic (R) Smoothness (A)", 2D) = "white" {}

		_BumpScale("Normal Scale", Range(0.0, 1.0)) = 1.0
		[Normal][NoScaleOffset] _BumpMap("Normal Map", 2D) = "bump" {}

		_OcclusionScale("Occlusion Scale", Range(0.0, 1.0)) = 1.0
		[NoScaleOffset] _OcclusionMap("Occlusion (G)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 400
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		// ======================================================================

		float4 _Color;
		sampler2D _MainTex;

		float _Glossiness;
		float _Metallic;
		sampler2D _MetallicGlossMap;

		float _BumpScale;
		sampler2D _BumpMap;

		float _OcclusionScale;
		sampler2D _OcclusionMap;

		// ======================================================================
		
		struct Input 
		{
			float2 uv_MainTex;
		};

		// ======================================================================

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			float4 diffTex = tex2D(_MainTex, IN.uv_MainTex);
			float4 msTex = tex2D(_MetallicGlossMap, IN.uv_MainTex);
			float4 nrmTex = tex2D(_BumpMap, IN.uv_MainTex);
			float4 aoTex = tex2D(_OcclusionMap, IN.uv_MainTex);

			o.Albedo = (diffTex * _Color).rgb;
			o.Metallic = msTex.r * _Metallic;
			o.Smoothness = msTex.a * _Glossiness;
			o.Normal = UnpackScaleNormal(nrmTex, _BumpScale);
			o.Occlusion = lerp(1, aoTex.g, _OcclusionScale);
		}

		// ======================================================================

		ENDCG
	} 

	FallBack "Diffuse"
}
